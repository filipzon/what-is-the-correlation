#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

STATE_CODES = {
    "AL": "Alabama", "AK": "Alaska", "AZ": "Arizona", "AR": "Arkansas",
    "CA": "California", "CO": "Colorado", "CT": "Connecticut",
    "DE": "Delaware", "DC": "District of Columbia", "FL": "Florida",
    "GA": "Georgia", "HI": "Hawaii", "ID": "Idaho", "IL": "Illinois",
    "IN": "Indiana", "IA": "Iowa", "KS": "Kansas", "KY": "Kentucky",
    "LA": "Louisiana", "ME": "Maine", "MD": "Maryland",
    "MA": "Massachusetts", "MI": "Michigan", "MN": "Minnesota",
    "MS": "Mississippi", "MO": "Missouri", "MT": "Montana",
    "NE": "Nebraska", "NV": "Nevada", "NH": "New Hampshire",
    "NJ": "New Jersey", "NM": "New Mexico", "NY": "New York",
    "NC": "North Carolina", "ND": "North Dakota", "OH": "Ohio",
    "OK": "Oklahoma", "OR": "Oregon", "PA": "Pennsylvania",
    "RI": "Rhode Island", "SC": "South Carolina", "SD": "South Dakota",
    "TN": "Tennessee", "TX": "Texas", "UT": "Utah", "VT": "Vermont",
    "VA": "Virginia", "WA": "Washington", "WV": "West Virginia",
    "WI": "Wisconsin", "WY": "Wyoming", "PR": "Puerto Rico"
}

EUROPEAN_UNION = [
    'AUT', 'BEL', 'BGR', 'HRV', 'CYP', 'CZE', 'DNK', 'EST', 'FIN', 'FRA',
    'DEU', 'GRC', 'HUN', 'IRL', 'ITA', 'LVA', 'LTU', 'LUX', 'MLT', 'NLD',
    'POL', 'PRT', 'ROU', 'SVK', 'SVN', 'ESP', 'SWE']

# russia, ukraine and vatican excluded
EUROPE_REST = [
    'ALB', 'AND', 'ARM', 'BLR', 'BIH', 'FRO', 'GEO', 'GIB', 'ISL', 'IMN',
    'XKX', 'LIE', 'MKD', 'MDA', 'MCO', 'MNE', 'NOR', 'SMR', 'SRB', 'CHE',
    'TUR', 'GBR']

EUROPE_TOTAL = EUROPEAN_UNION + EUROPE_REST

MISSING_CODES = {'England & Wales': 'GBR_ENG_WLS',
                 'Northern Ireland': 'GBR_NIR',
                 'Scotland': 'GBR_SCO',
                 'Wales': 'GBR_WLS',
                 'England': 'GBR_ENG'}

REPLACE_MAP = {'OWID_KOS': 'XKX'}


def fix_country_code(code, country_name):
    if code == '' and country_name in MISSING_CODES:
        code = MISSING_CODES[country_name]
    if code in REPLACE_MAP:
        return REPLACE_MAP[code]
    return code


def load_csv_lines(filepath):
    print(f'loading {filepath}')
    lines = []
    with open(filepath, "r") as f:
        for line in f:
            lines.append(line)
    return lines[1:-1]


def excess_mortality_from_ourworldindata():
    """
    loads excess mortality data downloaded from 
    https://ourworldindata.org/grapher/excess-mortality-p-scores-average-baseline
    """
    excess_mortality = defaultdict(lambda: {})
    
    lines = load_csv_lines('data/ourworldindata/excess-mortality-p-scores-average-baseline.csv')
    
    for line in lines:
        if line.startswith('Entity'):
            continue
        country_name, code, day, p_avg_all_ages = line.split(',')
        code = fix_country_code(code, country_name)
        if code:
            val = float(p_avg_all_ages)
            date = datetime.datetime.strptime(day, '%Y-%m-%d').date()
            excess_mortality[code][date] = val

    return excess_mortality


def excess_mortality_from_CDC():
    """
    loads data downloaded for each state from "Excess deaths with and without weighting" tab in 
    https://public.tableau.com/app/profile/dataviz8737/viz/COVID_excess_mort_withcauses_08232023/WeeklyExcessDeaths
    """
    def sanitize_line_for_split(_line):
        sanitized_line = ''
        between_quotation_marks = False
        for char in _line:
            if char == '"':
                between_quotation_marks = not between_quotation_marks
                continue
            if between_quotation_marks and char == ',':
                continue
            sanitized_line += char
        return sanitized_line

    excess_mortality_predicted = defaultdict(lambda: {})
    excess_mortality_observed = defaultdict(lambda: {})

    for code, state_name in STATE_CODES.items():
        average_expected = {}
        predicted = {}
        observed = {}

        lines = load_csv_lines('data/CDC/states/' + state_name + '.csv')
        lines.reverse()

        for line in lines:
            (observed_number, day, measure_names, type_, _, _, _, percent_excess_over_expected, _, _,
             avg_average_expected_number_of_deaths, _, _, measure_values) = sanitize_line_for_split(line).split(',')

            date = datetime.datetime.strptime(day, '%B %d %Y').date()

            if measure_names == 'Avg. Average Expected Number of Deaths':
                average_expected[date] = float(measure_values)

            if type_ == 'Predicted (weighted)':
                predicted[date] = float(observed_number)

            if type_ == 'Unweighted':
                observed[date] = float(observed_number)

        for date in observed:
            excess_mortality_observed[code][date] = round((observed[date] / average_expected[date] - 1) * 100, 1)

        for date in predicted:
            excess_mortality_predicted[code][date] = round((predicted[date] / average_expected[date] - 1) * 100, 1)

    return excess_mortality_predicted, excess_mortality_observed


def vaccination_from_ourworldindata():
    """
    loads vaccination rates downloaded from
    https://ourworldindata.org/grapher/covid-vaccination-doses-per-capita
    boosters may be also usefull
    https://ourworldindata.org/grapher/covid-vaccine-booster-doses-per-capita
    """
    excluded = ["Africa",
                "Asia",
                "Europe",
                "European Union",
                "High income",
                "Low income",
                "Lower middle income",
                "North America",
                "Oceania",
                "South America",
                "Upper middle income",
                "World excl. China"]

    vax_rate = defaultdict(lambda: {})

    lines = load_csv_lines('data/ourworldindata/covid-vaccination-doses-per-capita.csv')

    for line in lines:
        if line.startswith('Entity'):
            continue
        country_name, code, date, total_per_hundred = line.split(',')
        if country_name in excluded:
            continue
        code = fix_country_code(code, country_name)
        date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        vax_rate[code][date] = float(total_per_hundred)

    # not sure if excess mortality is weighted for merged England & Wales excess mort. data
    for date in vax_rate['GBR_ENG']:
        vax_rate['GBR_ENG_WLS'][date] = vax_rate['GBR_ENG'][date] + vax_rate['GBR_ENG'][date] / 2

    return vax_rate


def vaccination_from_CDC():
    """
    loads data downloaded from
    https://covid.cdc.gov/covid-data-tracker/#vaccinations
    can be verified with usafacts.org interpreataion
    https://usafacts.org/visualizations/covid-vaccine-tracker-states/
    """
    vax_rate = defaultdict(lambda: {})
    
    lines = load_csv_lines('data/CDC/COVID-19_Vaccinations_in_the_United_States_Jurisdiction.csv')
    lines.reverse()

    for line in lines:
        vals = line.split(',')
        admin_per_100k = vals[24]
        date = datetime.datetime.strptime(vals[0], '%m/%d/%Y').date()
        vax_rate[vals[2]][date] = float(admin_per_100k) / 1000
    return vax_rate


def interpolate(data):
    """ crude interpolation to daily resolution """
    print('interpolating')
    interpolated_data = defaultdict(lambda: {})
    for code in data:
        prev_date = None
        for date in data[code]:
            if prev_date:
                val = data[code][date]
                prev_val = data[code][prev_date]
                interval_in_dates = (date - prev_date).days
                delta = (val - prev_val) / interval_in_dates
                for _ in range(interval_in_dates - 1):
                    prev_date += datetime.timedelta(days=1)
                    prev_val += delta
                    interpolated_data[code][prev_date] = prev_val
            interpolated_data[code][date] = data[code][date]
            prev_date = date
    return interpolated_data


def cumulate(data):
    """ cumulation day-by-day """
    print('cumulating')
    cumulative_data = defaultdict(lambda: {})
    for code in data:
        prev_date = None
        for date in data[code]:
            if prev_date:
                cumulative_data[code][date] = cumulative_data[code][prev_date] + data[code][date]
            else:
                cumulative_data[code][date] = data[code][date]
            prev_date = date
    return cumulative_data


def round_pvalue(pvalue):
    """ rounds pvalue the way that significance is readable """
    if pvalue > 1:
        pvalue = round(pvalue, 1)
    else:
        temp = ''
        prev_char = '0'
        for char in f'{pvalue:.20f}':
            temp += char
            if prev_char not in ('0', '.'):
                pvalue = temp
                break
            prev_char = char
    return pvalue


def prepare_dataset(cumulative_excess_mortality, vaccination, codes, vax_day, mortality_window, range_size, trajectory):
    """ prepares dataset """
    print(f'\n{len(codes)} entries:')
    _x_points = []
    _y_points = []

    for CountryCode in codes:

        assert CountryCode in vaccination, print(f'{CountryCode} is not in vaccination data')

        if vax_day in vaccination[CountryCode]:
            vax_rate = vaccination[CountryCode][vax_day]
        else:
            last_vaccination_day = list(vaccination[CountryCode].keys())[-1]
            if vax_day > last_vaccination_day:
                vax_rate = vaccination[CountryCode][last_vaccination_day]
            else:
                continue

        if mortality_window[0] not in cumulative_excess_mortality[CountryCode]:
            continue
        if mortality_window[1] not in cumulative_excess_mortality[CountryCode]:
            continue

        deaths = round((cumulative_excess_mortality[CountryCode][mortality_window[1]] -
                        cumulative_excess_mortality[CountryCode][mortality_window[0]]) /
                       range_size, 2)

        _x_points.append(vax_rate)
        _y_points.append(deaths)

        trajectory[0][CountryCode].append(vax_rate)
        trajectory[1][CountryCode].append(deaths)

        print(f'{CountryCode},{round(vax_rate, 2)},{deaths}')

    return _x_points, _y_points


def compute_and_draw(plot, _x_points, _y_points, trajectory, label):
    """ computes correlations and feeds the plot """
    pearson = 'unknown'
    pvalue = 'unknown'
    if len(_x_points) >= 2:

        # can be verified at https://www.graphpad.com/quickcalcs/linear1/
        pearson, pvalue = scipy.stats.pearsonr(_x_points, _y_points)
        pearson = round(pearson, 2)
        pvalue = round_pvalue(pvalue)

        for code in trajectory[0]:
            plot.plot(trajectory[0][code],
                      trajectory[1][code],
                      color=(0.9, 0.9, 0.9),
                      linewidth=0.5)

        # regression
        try:
            a, b = np.polyfit(_x_points, _y_points, 1)
        except SystemError:
            pass
        else:
            plot.plot([min(_x_points), max(_x_points)],
                      [min(_x_points) * a + b, max(_x_points) * a + b])

    plot.plot(_x_points, _y_points, label=label, color=(0.7, 0, 0, 0.8), linestyle='', marker='o', markersize=4)
    plot.set(xlabel=f'doses per 100\n\npearson={pearson}, p-value={pvalue}')


if __name__ == '__main__':

    # countries
    world_excess_mortality = excess_mortality_from_ourworldindata()
    cumulative_world_excess_mortality = cumulate(interpolate(world_excess_mortality))
    world_vaccination_rates = vaccination_from_ourworldindata()
    world_vaccination_rates = interpolate(world_vaccination_rates)

    start_date = datetime.date(2021, 7, 1)
    window_size_in_days = 100
    step = 3

    range_a = start_date
    range_b = start_date + datetime.timedelta(days=window_size_in_days)

    counter = 0

    output_folder = f'output_{window_size_in_days}_{step}/'
    try:
        os.mkdir(output_folder)
    except FileExistsError as e:
        assert 'File exists' in str(e)

    trajectory1 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory2 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory3 = [defaultdict(lambda: []), defaultdict(lambda: [])]

    while range_b < datetime.date.today():
        range_a += datetime.timedelta(days=step)
        range_b += datetime.timedelta(days=step)

        vax_day = range_a + (range_b - range_a) / 2

        print(f'\n *** <{range_a},{range_b}>; {vax_day} ***')


        fig, (ax1, ax2,) = plt.subplots(1, 2, sharey=True)
        fig.set_figwidth(16)

        ax1.set(ylabel='excess mortality in %')

        ax1.set_title(f'Europe')


        ax1.set_xlim([0, 300])


        ax1.set_ylim([-20, 100])


        x_points, y_points = prepare_dataset(cumulative_world_excess_mortality,
                                            world_vaccination_rates,
                                            EUROPE_TOTAL,
                                            vax_day,
                                            [range_a, range_b],
                                            window_size_in_days,
                                            trajectory3)
        compute_and_draw(ax1, x_points, y_points, trajectory3, 'countries')

        fig.suptitle(f'excess mortality from <{range_a}, {range_b}> range, vaccination rate at {vax_day}')

        ax1.legend()


        plt.subplots_adjust(bottom=0.19)

        # plt.show()

        plt.savefig(f'{output_folder}{counter}_{vax_day}.png')
        plt.clf()
        counter += 1
