## What Is The Correlation

Visualizes correlation between vaccination rate and excess deaths.

Tested on Linux with:
- Python 3.10.12
- Matplotlib 3.7.1
- SciPy 1.10.0
- Numpy 1.23.1

# References and other materials

https://www.igor-chudov.com/p/association-between-vaccines-and
https://wherearethenumbers.substack.com/p/the-devils-advocate-an-exploratory

# Sources

ourworldindata.org
download -> Data -> Download full data
    https://ourworldindata.org/grapher/excess-mortality-p-scores-average-baseline
    https://ourworldindata.org/grapher/covid-vaccination-doses-per-capita
    https://ourworldindata.org/grapher/covid-vaccine-booster-doses-per-capita


https://www.euromomo.eu/graphs-and-maps/

# TODO

Przyjrzec sie jeszcze czy dobra vax rate wartosci biore od cdc

przetworzyc dane stad

https://ourworldindata.org/us-states-vaccinations

https://ourworldindata.org/covid-vaccinations

https://ourworldindata.org/grapher/covid-fully-vaccinated-by-age
