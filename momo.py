#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import defaultdict
import os
import datetime
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats

STATE_CODES = {
    "AL": "Alabama", "AK": "Alaska", "AZ": "Arizona", "AR": "Arkansas",
    "CA": "California", "CO": "Colorado", "CT": "Connecticut",
    "DE": "Delaware", "DC": "District of Columbia", "FL": "Florida",
    "GA": "Georgia", "HI": "Hawaii", "ID": "Idaho", "IL": "Illinois",
    "IN": "Indiana", "IA": "Iowa", "KS": "Kansas", "KY": "Kentucky",
    "LA": "Louisiana", "ME": "Maine", "MD": "Maryland",
    "MA": "Massachusetts", "MI": "Michigan", "MN": "Minnesota",
    "MS": "Mississippi", "MO": "Missouri", "MT": "Montana",
    "NE": "Nebraska", "NV": "Nevada", "NH": "New Hampshire",
    "NJ": "New Jersey", "NM": "New Mexico", "NY": "New York",
    "NC": "North Carolina", "ND": "North Dakota", "OH": "Ohio",
    "OK": "Oklahoma", "OR": "Oregon", "PA": "Pennsylvania",
    "RI": "Rhode Island", "SC": "South Carolina", "SD": "South Dakota",
    "TN": "Tennessee", "TX": "Texas", "UT": "Utah", "VT": "Vermont",
    "VA": "Virginia", "WA": "Washington", "WV": "West Virginia",
    "WI": "Wisconsin", "WY": "Wyoming", "PR": "Puerto Rico"
}

EUROPEAN_UNION = [
    'AUT', 'BEL', 'BGR', 'HRV', 'CYP', 'CZE', 'DNK', 'EST', 'FIN', 'FRA',
    'DEU', 'GRC', 'HUN', 'IRL', 'ITA', 'LVA', 'LTU', 'LUX', 'MLT', 'NLD',
    'POL', 'PRT', 'ROU', 'SVK', 'SVN', 'ESP', 'SWE']

# russia, ukraine and vatican excluded
EUROPE_REST = [
    'ALB', 'AND', 'ARM', 'BLR', 'BIH', 'FRO', 'GEO', 'GIB', 'ISL', 'IMN',
    'XKX', 'LIE', 'MKD', 'MDA', 'MCO', 'MNE', 'NOR', 'SMR', 'SRB', 'CHE',
    'TUR', 'GBR']

EUROPE_TOTAL = EUROPEAN_UNION + EUROPE_REST

MISSING_CODES = {'England & Wales': 'GBR_ENG_WLS',
                 'Northern Ireland': 'GBR_NIR',
                 'Scotland': 'GBR_SCO',
                 'Wales': 'GBR_WLS',
                 'England': 'GBR_ENG'}

CODES_FOR_EUROMOMO = {  'Denmark': 'DNK',
                        'Belgium': 'BEL',
                        'Austria': 'AUT',
                        'Cyprus': 'CYP',
                        'Estonia': 'EST',
                        'Finland': 'FIN',
                        'Germany': 'DEU',
                        'UK (England)': 'GBR_ENG',
                        'Sweden': 'SWE',
                        'Spain': 'ESP',
                        'Portugal': 'PRT',
                        'Netherlands': 'NLD',
                        'Malta': 'MLT',
                        'Luxembourg': 'LUX',
                        'Israel': 'ISR',
                        'Ireland': 'IRL',
                        'Greece': 'GRC',
                        'Germany (Hesse)': '',
                        'Italy': 'ITA',
                        'Slovenia': 'SVN',
                        'Switzerland': 'CHE',
                        'UK (Northern Ireland)': 'GBR_NIR',
                        'Hungary': 'HUN',
                        'UK (Scotland)': 'GBR_SCO',
                        'Germany (Berlin)': '',
                        'France': 'FRA'}

REPLACE_MAP = {'OWID_KOS': 'XKX'}

def fix_country_code(code, country_name):
    if code == '':
        code = MISSING_CODES[country_name]
    if code in REPLACE_MAP:
        return REPLACE_MAP[code]
    return code


def load_csv_lines(filepath):
    lines = []
    with open(filepath, "r") as f:
        for line in f:
            lines.append(line)
    return lines[1:-1]


def excess_mortality_from_ourworldindata():
    """
    loads excess mortality data downloaded from 
    https://ourworldindata.org/grapher/excess-mortality-p-scores-average-baseline
    """
    print('loading')
    
    excess_mortality = defaultdict(lambda: {})
    
    lines = load_csv_lines('data/ourworldindata/excess-mortality-p-scores-average-baseline.csv')
    
    for line in lines:
        country_name, code, day, p_avg_all_ages = line.split(',')
        code = fix_country_code(code, country_name)
        val = float(p_avg_all_ages)
        date = datetime.datetime.strptime(day, '%Y-%m-%d').date()
        excess_mortality[code][date] = val

    return excess_mortality


def excess_mortality_from_euromomo(age_group):
    """
    loads age stratified excess mortality data downloaded from 
    https://www.euromomo.eu/graphs-and-maps/
    """
    print('loading')
    
    excess_mortality = defaultdict(lambda: {})
    
    lines = load_csv_lines(f'data/euromomo/{age_group}.csv')
    
    for line in lines:
        country_name, group, year_week, zscore = line.split(';')
        assert age_group == group
        if(country_name in ['Germany (Hesse)', 'Germany (Berlin)']): continue
        code = CODES_FOR_EUROMOMO[country_name]
        val = float(zscore)
        year, week = year_week.split('-')
        date = datetime.date.fromisocalendar(int(year), int(week), 3)
        excess_mortality[code][date] = val * 10  # TODO odwrocic z-score na procenty

    return excess_mortality


def excess_mortality_from_CDC():
    """
    loads data downloaded for each state from "Excess deaths with and without weighting" tab in 
    https://public.tableau.com/app/profile/dataviz8737/viz/COVID_excess_mort_withcauses_08232023/WeeklyExcessDeaths
    """
    print('loading')

    def sanitize_line_for_split(_line):
        sanitized_line = ''
        between_quotation_marks = False
        for char in _line:
            if char == '"':
                between_quotation_marks = not between_quotation_marks
                continue
            if between_quotation_marks and char == ',':
                continue
            sanitized_line += char
        return sanitized_line

    excess_mortality_predicted = defaultdict(lambda: {})
    excess_mortality_observed = defaultdict(lambda: {})

    for code, state_name in STATE_CODES.items():
        average_expected = {}
        predicted = {}
        observed = {}

        lines = load_csv_lines('data/CDC/states/' + state_name + '.csv')
        lines.reverse()

        for line in lines:
            (observed_number, day, measure_names, type_, _, _, _, percent_excess_over_expected, _, _,
             avg_average_expected_number_of_deaths, _, _, measure_values) = sanitize_line_for_split(line).split(',')

            date = datetime.datetime.strptime(day, '%B %d %Y').date()

            if measure_names == 'Avg. Average Expected Number of Deaths':
                average_expected[date] = float(measure_values)

            if type_ == 'Predicted (weighted)':
                predicted[date] = float(observed_number)

            if type_ == 'Unweighted':
                observed[date] = float(observed_number)

        for date in observed:
            excess_mortality_observed[code][date] = round((observed[date] / average_expected[date] - 1) * 100, 1)

        for date in predicted:
            excess_mortality_predicted[code][date] = round((predicted[date] / average_expected[date] - 1) * 100, 1)

    return excess_mortality_predicted, excess_mortality_observed


def vaccination_from_ourworldindata():
    """
    loads vaccination rates downloaded from
    https://ourworldindata.org/grapher/covid-vaccination-doses-per-capita
    boosters may be also usefull
    https://ourworldindata.org/grapher/covid-vaccine-booster-doses-per-capita
    """
    print('loading')
    excluded = ["Africa",
                "Asia",
                "Europe",
                "European Union",
                "High income",
                "Low income",
                "Lower middle income",
                "North America",
                "Oceania",
                "South America",
                "Upper middle income",
                "World excl. China"]

    vax_rate = defaultdict(lambda: {})

    lines = load_csv_lines('data/ourworldindata/covid-vaccination-doses-per-capita.csv')

    for line in lines:
        if line.startswith('Entity'):
            continue
        country_name, code, date, total_per_hundred = line.split(',')
        if country_name in excluded:
            continue
        code = fix_country_code(code, country_name)
        date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        vax_rate[code][date] = float(total_per_hundred)

    # not sure if excess mortality is weighted for merged England & Wales excess mort. data
    for date in vax_rate['GBR_ENG']:
        vax_rate['GBR_ENG_WLS'][date] = vax_rate['GBR_ENG'][date] + vax_rate['GBR_ENG'][date] / 2

    return vax_rate


def vaccination_from_CDC():
    """
    loads data downloaded from
    https://covid.cdc.gov/covid-data-tracker/#vaccinations
    can be verified with usafacts.org interpreataion
    https://usafacts.org/visualizations/covid-vaccine-tracker-states/
    """
    print('loading')
    
    vax_rate = defaultdict(lambda: {})
    
    lines = load_csv_lines('data/CDC/COVID-19_Vaccinations_in_the_United_States_Jurisdiction.csv')
    lines.reverse()

    for line in lines:
        vals = line.split(',')
        admin_per_100k = vals[24]
        date = datetime.datetime.strptime(vals[0], '%m/%d/%Y').date()
        vax_rate[vals[2]][date] = float(admin_per_100k) / 1000
    return vax_rate


def interpolate(data):
    """ crude interpolation to daily resolution """
    print('interpolating')
    interpolated_data = defaultdict(lambda: {})
    for code in data:
        prev_date = None
        for date in data[code]:
            if prev_date:
                val = data[code][date]
                prev_val = data[code][prev_date]
                interval_in_dates = (date - prev_date).days
                delta = (val - prev_val) / interval_in_dates
                for _ in range(interval_in_dates - 1):
                    prev_date += datetime.timedelta(days=1)
                    prev_val += delta
                    interpolated_data[code][prev_date] = prev_val
            interpolated_data[code][date] = data[code][date]
            prev_date = date
    return interpolated_data


def cumulate(data):
    """ cumulation day-by-day """
    print('cumulating')
    cumulative_data = defaultdict(lambda: {})
    for code in data:
        prev_date = None
        for date in data[code]:
            if prev_date:
                cumulative_data[code][date] = cumulative_data[code][prev_date] + data[code][date]
            else:
                cumulative_data[code][date] = data[code][date]
            prev_date = date
    return cumulative_data


def round_pvalue(pvalue):
    """ rounds pvalue the way that significance is readable """
    if pvalue > 1:
        pvalue = round(pvalue, 1)
    else:
        temp = ''
        prev_char = '0'
        for char in f'{pvalue:.20f}':
            temp += char
            if prev_char not in ('0', '.'):
                pvalue = temp
                break
            prev_char = char
    return pvalue


def prepare_points(cumulative_excess_mortality, vaccination, codes, vax_day, range_size, trajectory):
    """ prepares dataset for correlation """
    print(f'\ncorrelation for {len(codes)} entries')
    _x_points = []
    _y_points = []

    for CountryCode in codes:

        assert CountryCode in vaccination, print(f'{CountryCode} is not in vaccination data')

        if vax_day in vaccination[CountryCode]:
            vax_rate = vaccination[CountryCode][vax_day]
        else:
            last_vaccination_day = list(vaccination[CountryCode].keys())[-1]
            if vax_day > last_vaccination_day:
                vax_rate = vaccination[CountryCode][last_vaccination_day]
            else:
                continue

        if range_a not in cumulative_excess_mortality[CountryCode]:
            continue
        if range_b not in cumulative_excess_mortality[CountryCode]:
            continue

        deaths = round((cumulative_excess_mortality[CountryCode][range_b] -
                        cumulative_excess_mortality[CountryCode][range_a]) /
                       range_size, 2)

        _x_points.append(vax_rate)
        _y_points.append(deaths)

        trajectory[0][CountryCode].append(vax_rate)
        trajectory[1][CountryCode].append(deaths)

        print(f'{CountryCode},{round(vax_rate, 2)},{deaths}')

    return _x_points, _y_points


def compute_and_draw(plot, _x_points, _y_points, trajectory, label):
    """ computes correlations and feeds the plot """
    pearson = 'unknown'
    pvalue = 'unknown'
    if len(_x_points) >= 2:

        # can be verified at https://www.graphpad.com/quickcalcs/linear1/
        pearson, pvalue = scipy.stats.pearsonr(_x_points, _y_points)
        pearson = round(pearson, 2)
        pvalue = round_pvalue(pvalue)

        for code in trajectory[0]:
            plot.plot(trajectory[0][code],
                      trajectory[1][code],
                      color=(0.9, 0.9, 0.9),
                      linewidth=0.5)

        # regression
        try:
            a, b = np.polyfit(_x_points, _y_points, 1)
        except SystemError:
            pass
        else:
            plot.plot([min(_x_points), max(_x_points)],
                      [min(_x_points) * a + b, max(_x_points) * a + b])

    plot.plot(_x_points, _y_points, label=label, color=(0.7, 0, 0, 0.8), linestyle='', marker='o', markersize=4)
    plot.set(xlabel=f'doses per 100\npearson={pearson}, p-value={pvalue}')


if __name__ == '__main__':


    # states, CDC
    US_excess_mortality_predicted, US_excess_mortality_observed = excess_mortality_from_CDC()
    cumulative_US_excess_mortality_predicted = cumulate(interpolate(US_excess_mortality_predicted))
    cumulative_US_excess_mortality_observed = cumulate(interpolate(US_excess_mortality_observed))
    US_vaccination_rates = vaccination_from_CDC()
    US_vaccination_rates = interpolate(US_vaccination_rates)

    # world, ourworldindata.org
    world_excess_mortality = excess_mortality_from_ourworldindata()
    cumulative_world_excess_mortality = cumulate(interpolate(world_excess_mortality))
    world_vaccination_rates = vaccination_from_ourworldindata()
    world_vaccination_rates = interpolate(world_vaccination_rates)

    # europe, euromomo
    europe_excess_mortality_euromomo = excess_mortality_from_euromomo(age_group='Total')
    cumulative_europe_excess_mortality_euromomo = cumulate(interpolate(europe_excess_mortality_euromomo))

    start_date = datetime.date(2020, 7, 1)
    window_size_in_days = 100
    step = 3

    range_a = start_date
    range_b = start_date + datetime.timedelta(days=window_size_in_days)

    counter = 0

    output_folder = f'output_{window_size_in_days}_{step}/'
    try:
        os.mkdir(output_folder)
    except FileExistsError as e:
        assert 'File exists' in str(e)

    trajectory1 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory2 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory3 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory4 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory5 = [defaultdict(lambda: []), defaultdict(lambda: [])]
    trajectory6 = [defaultdict(lambda: []), defaultdict(lambda: [])]

    while range_b < datetime.date.today():
        range_a += datetime.timedelta(days=step)
        range_b += datetime.timedelta(days=step)

        the_day = range_a + (range_b - range_a) / 2

        print(f'\n{the_day} +/- {window_size_in_days}:')

        fig, _ax = plt.subplots(2, 3, sharey=True)
        ax = [_ax[0][0], _ax[0][1],_ax[0][2], _ax[1][0], _ax[1][1],_ax[1][2]]
        
        fig.set_figwidth(16)
        fig.set_figheight(13)

        ax[0].set(ylabel='excess mortality in %')

        ax[0].set_title(f'USA')
        ax[1].set_title(f'Europe')
        ax[2].set_title(f'World')
        ax[3].set_title(f'Europe 0-14')
        ax[4].set_title(f'Europe 15-44')
        ax[5].set_title(f'Europe 45-64')

        for _ax in ax:
            _ax.set_xlim([0, 300])
            _ax.set_ylim([-20, 100])

        fig.suptitle(f'excess deaths from {range_a} to {range_b} period, vaccination rate at {the_day}')

        # not sure if to use predicted or observed
        x_points, y_points = prepare_points(cumulative_US_excess_mortality_observed,
                                            US_vaccination_rates,
                                            cumulative_US_excess_mortality_observed.keys(),
                                            the_day,
                                            window_size_in_days,
                                            trajectory1)
        compute_and_draw(ax[0], x_points, y_points, trajectory1, 'states')

        x_points, y_points = prepare_points(cumulative_world_excess_mortality,
                                            world_vaccination_rates,
                                            EUROPE_TOTAL,
                                            the_day,
                                            window_size_in_days,
                                            trajectory2)
        compute_and_draw(ax[1], x_points, y_points, trajectory2, 'countries')

        x_points, y_points = prepare_points(cumulative_world_excess_mortality,
                                            world_vaccination_rates,
                                            world_vaccination_rates.keys(),
                                            the_day,
                                            window_size_in_days,
                                            trajectory3)
        compute_and_draw(ax[2], x_points, y_points, trajectory3, 'countries')

        x_points, y_points = prepare_points(cumulative_europe_excess_mortality_euromomo,
                                            world_vaccination_rates,
                                            cumulative_europe_excess_mortality_euromomo.keys(),
                                            the_day,
                                            window_size_in_days,
                                            trajectory4)
        compute_and_draw(ax[3], x_points, y_points, trajectory4, 'countries')

        # ~ x_points, y_points = prepare_points(cumulative_europe_excess_mortality_age_15to44,
                                            # ~ world_vaccination_rates,
                                            # ~ cumulative_europe_excess_mortality_age_15to44.keys(),
                                            # ~ the_day,
                                            # ~ window_size_in_days,
                                            # ~ trajectory5)
        # ~ compute_and_draw(ax[4], x_points, y_points, trajectory5, 'countries')

        # ~ x_points, y_points = prepare_points(cumulative_europe_excess_mortality_age_45to64,
                                            # ~ world_vaccination_rates,
                                            # ~ cumulative_europe_excess_mortality_age_45to64.keys(),
                                            # ~ the_day,
                                            # ~ window_size_in_days,
                                            # ~ trajectory6)
        # ~ compute_and_draw(ax[5], x_points, y_points, trajectory6, 'countries')


        for _ax in ax:
            _ax.legend()

        plt.subplots_adjust(bottom=0.19)

        # ~ plt.show()

        plt.savefig(output_folder + str(counter) + '.png')
        plt.clf()
        counter += 1
